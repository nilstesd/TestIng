from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from selenium.webdriver.chrome.options import Options

options = Options()
#options.add_argument("--headless")
driver = webdriver.Chrome(chrome_options=options)
driver.get("https://www.digitalarkivet.no/")
time.sleep(1)

#assert "Logg inn i banken" in driver.title
#time.sleep(1)

elem = driver.find_element_by_id("digitalarkivet-text-logo")

elem = driver.find_element_by_link_text("Avansert personsøk")
elem.click()

time.sleep(1)

elem = driver.find_element_by_name("firstname")
elem.send_keys("Karoline")



elem = driver.find_element_by_name("lastname")
elem.send_keys("Nilsen")

time.sleep(1)

elem = driver.find_element_by_name("birth_year_from")
elem.send_keys("1857")


elem = driver.find_element_by_name("birth_year_to")
elem.send_keys("1857")


elem = driver.find_element_by_name("birth_date")
elem.send_keys("09-01")

elem = driver.find_element_by_class_name("btn-primary")
elem.click()

time.sleep(1)

#elem = driver.find_element_by_id("viewModeToggle")
#elem.click()

time.sleep(1)
elem = driver.find_element_by_class_name("block-link")
elem.click()

time.sleep(1)

#elem = driver.find_element_by_link_text("Karoline Nilsen")
#elem.click()


liste = driver.find_elements_by_class_name("data-item")
print(len(liste))

elem = driver.find_element_by_link_text("0029 Storgate 33")
elem.click()

time.sleep(1)

liste = driver.find_elements_by_class_name("ssp-semibold")
print(liste[4].text)

#driver.close()